declare module Core {
    export interface ICache {
        set<T>(key: string, value: T): T;
        get<T>(key: string): T;
        has(key: string): boolean;
        remove(key: string): void;
        clear(): void;
    }
}
declare module Core {
    
    export interface IDictionary<T,K> {
        set(key: K, value: T): T;

        get(key: K, copy?: boolean): T;

        has(key: K): boolean;

        keys(): K[];

        values(): T[];

        remove(key: K): T;

        clear(): void;
    }
}
declare module Core {
    class Color {
        r: number;
        g: number;
        b: number;
        constructor(r: number, g: number, b: number);
        /**
        * @param {ratio:number} zakres 0 - 1
        */
        changeValue: (ratio: number) => void;
        toHex: () => string;
        static fromHex: (hex: string) => Color;
    }
}

declare module Core {
    class Dictionary<T> implements IDictionary<T, number> {
        private _map;
        private _keys;
        private _values;
        set: (key: number, value: T) => T;
        get: (key: number, copy?: boolean) => T;
        has: (key: number) => boolean;
        keys: () => number[];
        values: () => T[];
        remove: (key: number) => T;
        clear: () => void;
    }
}

declare module Core {
    module ExternalData {
    }
}

declare module Core {
    function Guid(): string;
}

declare module Core {
    class InMemoryCache implements ICache {
        private data;
        set: <T>(key: string, value: T) => T;
        get: <T>(key: string) => T;
        has: (key: string) => boolean;
        remove: (key: string) => void;
        clear: () => void;
        private createMap;
    }
}

declare module Core {
    class LocalStorageCache implements ICache {
        set: <T>(key: string, value: T) => T;
        get: <T>(key: string) => T;
        has: (key: string) => boolean;
        remove: (key: string) => void;
        clear: () => void;
    }
}

declare module Core {
    class StorageDictionary<T> implements IDictionary<T, number> {
        static SEPARATOR: string;
        static STORAGENAME: string;
        static INSTANCES: {
            [name: string]: StorageDictionary<any>;
        };
        private _map;
        private _keys;
        private _values;
        private _cacheKeys;
        private localCache;
        private enityName;
        private idsKey;
        private userPrefix;
        private deserializationFunction;
        constructor(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T);
        static getInstance: <T>(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T) => StorageDictionary<T>;
        static generateInstanceName: (userId: number | string, listName: string, entityName: string) => string;
        private setIndicesToCache;
        private getIndices;
        private getKey;
        private loadEntityFromCache;
        set: (key: number, value: T, addToCache?: boolean) => T;
        get: (key: number) => T;
        has: (key: number) => boolean;
        keys: () => number[];
        values: () => T[];
        remove: (key: number, removeFromCache?: boolean) => T;
        clear: () => void;
    }
}

declare module Core {
    class StorageDictionaryString<T> implements IDictionary<T, string> {
        static SEPARATOR: string;
        static STORAGENAME: string;
        static INSTANCES: {
            [name: string]: StorageDictionaryString<any>;
        };
        private _map;
        private _keys;
        private _values;
        private _cacheKeys;
        private localCache;
        private enityName;
        private idsKey;
        private userPrefix;
        private deserializationFunction;
        constructor(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T);
        static getInstance: <T>(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T) => StorageDictionaryString<T>;
        static generateInstanceName: (userId: number | string, listName: string, entityName: string) => string;
        private setIndicesToCache;
        private getIndices;
        private getKey;
        private loadEntityFromCache;
        set: (key: string, value: T, addToCache?: boolean) => T;
        get: (key: string) => T;
        has: (key: string) => boolean;
        keys: () => string[];
        values: () => T[];
        remove: (key: string, removeFromCache?: boolean) => T;
        clear: () => void;
    }
}

declare module Core {
    class StringDictionary<T> implements IDictionary<T, string> {
        private _map;
        private _keys;
        private _values;
        set: (key: string, value: T) => T;
        get: (key: string, copy?: boolean) => T;
        has: (key: string) => boolean;
        keys: () => string[];
        values: () => T[];
        remove: (key: string) => T;
        clear: () => void;
    }
}
