/// <reference path="../../../../src/ts/common.d.ts" />

describe("Color", () => {
  var color: Core.Color;

  beforeEach(() => {
    color = new Core.Color(100, 100, 100);
  });

  var checkRgbValue = (color: Core.Color, rVal: number, gVal: number, bVal: number) => {
    expect(color.r).toBe(rVal);
    expect(color.b).toBe(bVal);
    expect(color.g).toBe(gVal);
  }

  it('should set rgb', () => {
    color.r = 100, color.b = 100, color.g = 100;
    checkRgbValue(color, 100, 100, 100);
  });

  it('should return hex color', () => {
    var hexColor = color.toHex();
    expect(hexColor).toBe('#646464');
  });

  it('should return new object with rgb color', () => {
    var newColor = Core.Color.fromHex(color.toHex());
    checkRgbValue(newColor, color.r, color.g, color.b);

    var newColorEmpty = Core.Color.fromHex('');
    checkRgbValue(newColorEmpty, 0, 0, 0);
  });

  it('should change color value', () => {
    color.changeValue(0.33);
    checkRgbValue(color, 33, 33, 33);
  });
});