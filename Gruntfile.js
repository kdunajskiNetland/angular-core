'use strict';
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    karma: {
      options: {
        configFile: 'conf/karma.conf.js'
      },
      dev: {
        exclude: [
          //'external/promise-polyfill/Promise.js'
        ]
      },
      ci: {
        browsers: ['Chrome'],
        coverageReporter: {
          type: 'cobertura',
          dir: 'dist/coverage/'
        }
      }
    },

    tsd: {
      refresh: {
        options: {
          command: 'reinstall',
          latest: false,
          config: 'tsd.json'
        }
      }
    },
    processhtml: {
      options: {
        data: {
        }
      },
      release: {
        files: {
          'dist/release/main.html': ['src/main.html']
        }
      }
    },
    // Bower plugin
    "bower-install-simple": {
      options: {
        color: true,
        directory: 'external',
        update: true,
        command: "update"
      },
      dev: {
      }
    },
    clean: {
      declaration: [
        "src/ts/core/typings/**/*.js"
      ],
      allDeclaration: ["src/ts/core/typings/**/*.*"],
      jsFromSrc: ["src/ts/**/*.js"]

    },
    lineremover: {
      customExclude: {
        files: {
          'dist/release/angular-core.d.ts': 'dist/release/angular-core.d.ts'
        },
        options: {
          exclusionPattern: /\/\/\/ \<ref/g
        }
      },
    },
    'sanitize': {
      options: {
      },
      files: {
        src: [
          'dist/build/ts/**/*.js'
        ]
      }
    }
  });
  grunt.loadTasks('tasks');

  grunt.registerTask('styles', [
    "sass:build"
  ]);

  grunt.registerTask('declaration', [
    'typescript:declaration',
    'clean:declaration'
  ]);

  grunt.registerTask('scripts', [
    'clean:allDeclaration',
    'typescript:build',
    'declaration',
    'sanitize'

  ]);

  grunt.registerTask('external', [
    'bower-install-simple',
    'copy:external',
  ]);

  grunt.registerTask('default', [
    'dev'
  ]);

  grunt.registerTask('dev', [
    'external',
    'tsd',
    'styles',
    'scripts',
    'copy:initBuild',
    //'tests'
  ]);

  grunt.registerTask('release', [
    'dev',
    'concat:csr',
    'concat:csrDeclaration',
    'lineremover'    
    //'uglify'
  ]);

  grunt.registerTask('deploy', [
    'release',
    'copy:deploy'
  ]);

}
