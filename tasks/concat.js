var concat = {
  options: {
    stripBanners: true
  },
  csr: {
    src: [
      "dist/build/ts/core/common/dictionary.js",
      "dist/build/ts/core/common/externaldata.js",
      "dist/build/ts/core/common/guid.js",
      "dist/build/ts/core/common/storagedictionary.js",
      "dist/build/ts/core/common/storagedictionarystring.js",
      "dist/build/ts/core/common/stringdictionary.js",
      "dist/build/ts/core/common/inmemorycache.js",
	  "dist/build/ts/core/common/localstoragecache.js",
	  "dist/build/ts/core/coreModule.js",
    ],
    dest: 'dist/release/angular-core.js',
    nonull: true
  },
  csrDeclaration: {
    src: [
      "src/ts/core/common/iCache.d.ts",
      "src/ts/core/core.d.ts",
      "src/ts/core/typings/**/*.ts",
    ],
    dest: 'dist/release/angular-core.d.ts',
    nonull: true
  },
  core: {
    src: [
      "src/ts/core/services/iCache.d.ts",
      "src/ts/core/core.d.ts",
    ],
    dest: 'dist/release/coreModule.js',
    nonull: true
  }
}
module.exports = function (grunt) {
  grunt.config.set('concat', concat);
}
