var watch = {
  ts: {
    files: ['src/ts/**/*.ts', '!src/ts/csr/typings/*.*'],
    tasks: ['clean:allDeclaration', 'typescript:build'],
    options: {
      livereload: true,
      debug: false,
      debounceDelay: 100
    }
  },
  declaration: {
    files: ['src/ts/csr/services/events.ts'],
    tasks: ['declaration'],
    options: {
      debug: false,
      debounceDelay: 100
    }
  },  
  sass: {
    files: ['src/sass/**/*.scss'],
    tasks: ['styles'],
    options: {
      livereload: true,
      debug: false,
      debounceDelay: 100
    }
  },
  html: {
    files: ['src/**/*.html'],
    tasks: ['copy:htmlBuild'],
    options: {
      livereload: true,
      debug: false,
      debounceDelay: 100
    }
    
  },
  tests: {
    files: ['tests/**/*.ts'],
    tasks: ['typescript:tests','clean:jsFromSrc','karma:ci'],
    options: {
      debug: false,
      debounceDelay: 100
    }
    
  }
}
module.exports = function (grunt) {
  grunt.config.set('watch', watch);
}
