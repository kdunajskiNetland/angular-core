var copy = {
  external: {
    files: [
      { expand: true, cwd: 'external/angular/', src: ['angular.min.js', 'angular.js'], dest: 'src/lib/angular' },
      { expand: true, cwd: 'external/angular-route/', src: ['angular-route.js', 'angular-route.min.js'], dest: 'src/lib/angular' },
      { expand: true, cwd: 'external/angular-mocks/', src: ['angular-mocks.js', 'ngAnimateMock.js', 'nkMock.js', 'ngMockE2E.js'], dest: 'src/lib/angular-tests' },
      { expand: true, cwd: 'external/jquery/dist', src: ['*.js'], dest: 'src/lib/jquery' },
      
    ]
  },
  release: {
    files: [
      { expand: true, cwd: 'src/typings/angular-wv-core.d.ts', src: [
          '',
        ], dest: 'dist/release/typings'
      },
    ]
  },
  initBuild: {
    files: [
      { expand: true, cwd: 'src', src: ['**', '!**/*.ts', '!**/*.scss'], dest: 'dist/build' },
    ]
  },
  htmlBuild: {
    files: [
      { expand: true, cwd: 'src', src: ['**/*.html'], dest: 'dist/build' },
    ]
  }
};

module.exports = function (grunt) {
  grunt.config.set('copy', copy);
}
