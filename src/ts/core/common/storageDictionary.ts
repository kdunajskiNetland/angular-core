/// <reference path="../../common.d.ts" />
module Core {
    export class StorageDictionary<T> implements IDictionary<T, number> {
        static SEPARATOR: string = "#";
        static STORAGENAME: string = "storage";
        static INSTANCES: {[name:string]:StorageDictionary<any>} = {};

        private _map: any = {};
        private _keys: number[] = [];
        private _values: T[] = [];
        private _cacheKeys: number[] = [];
        private localCache: Core.LocalStorageCache;
        private enityName:string;
        private idsKey: string;
        private userPrefix:string;
        private deserializationFunction: (obj: any) => T;


        constructor(userId:number | string, listName: string, entityName: string, deserializationFunction?: (obj:any) => T){
            var instanceName = StorageDictionary.generateInstanceName(userId, listName, entityName);
            
            if(StorageDictionary.INSTANCES.hasOwnProperty(instanceName)){
                throw new Error("rror: Instantiation failed: Use StorageDictionary.getInstance() instead of new.");
            }
            
            this.localCache = new LocalStorageCache();
            this.enityName = entityName;
            this.userPrefix = StorageDictionary.STORAGENAME + StorageDictionary.SEPARATOR + userId.toString();
            this.idsKey = this.userPrefix + StorageDictionary.SEPARATOR + listName;
            this.deserializationFunction = deserializationFunction;
            this.loadEntityFromCache();
            
            var instanceName = StorageDictionary.generateInstanceName(userId, listName, entityName);
            
            StorageDictionary.INSTANCES[instanceName] = this;
            
        }

        public static getInstance = <T>(userId:number | string, listName: string, entityName: string, deserializationFunction?: (obj:any) => T):StorageDictionary<T> => {
            var instanceName = StorageDictionary.generateInstanceName(userId, listName, entityName);
            if(StorageDictionary.INSTANCES.hasOwnProperty(instanceName)){
                return StorageDictionary.INSTANCES[instanceName];
            }else{
                return new StorageDictionary(userId, listName, entityName, deserializationFunction);
            }
        }
        
        public static generateInstanceName = (userId:number | string, listName: string, entityName: string):string => {
            return userId.toString() + "-" + listName + "-" + entityName;
        }

        private setIndicesToCache = (): void => {
            this.localCache.set(this.idsKey, this._keys);
        }

        private getIndices = (): number[] => {
            if (this._cacheKeys.length > 0) {
                return this._cacheKeys;
            }

            return this.localCache.get<number[]>(this.idsKey);
        }

        private getKey = (entityId: number): string => {
            return this.idsKey + StorageDictionary.SEPARATOR + this.enityName + StorageDictionary.SEPARATOR + entityId;
        }


        private loadEntityFromCache = (): void => {

            this._cacheKeys = this.getIndices();
            if(this._cacheKeys){
                for (var id of this._cacheKeys) {
                    var entity:any
                    if(this.deserializationFunction !== undefined && this.deserializationFunction !== null){
                        entity = this.deserializationFunction(this.localCache.get<T>(this.getKey(id)));
                    }else{
                        entity = this.localCache.get<T>(this.getKey(id));
                    }

                    this.set(id, entity, false);

                }
            }else{
                this._cacheKeys = [];
            }
        }

        public set = (key: number, value: T, addToCache: boolean = true): T => {
            var item = this._map[key];

            this._map[key] = value;

            if (item == null) {
                this._keys.push(key);

                this._values.push(value);
            } else {
                var index = this._keys.indexOf(key);

                if (index != -1) {
                    this._values[index] = value;
                }
            }

            if(addToCache){
                this.localCache.set(this.getKey(key), value);
                this._cacheKeys.push(key);
                this.setIndicesToCache();
            }

            return value;
        }

        get = (key: number): T => {
            return this._map[key];
        }

        has = (key: number): boolean => {
            return this._map[key] ? true : false;
        }

        keys = (): number[] => {
            return this._keys;
        }

        values = (): T[] => {
            return this._values;
        }

        remove = (key: number, removeFromCache: boolean = true): T => {
            var item = this._map[key];

            if (item == null) return;

            this._map[key] = null;

            var index = this._values.indexOf(item);

            if (index == -1) return;

            this._keys.splice(index, 1);

            if(removeFromCache){
                var cacheIndex:number = this._cacheKeys.indexOf(key);
                this.localCache.remove(this.getKey(key));
                this._cacheKeys.splice(cacheIndex, 1);
                this.setIndicesToCache();
            }

            return this._values.splice(index, 1)[0];
        }

        clear = (): void => {
            this._map = Object.create(null);

            this._values = [];

            this._keys = [];

            for (var index = 0, len = this._cacheKeys.length; index < len; index++) {
                var key = this._cacheKeys[index];
                this.localCache.remove(this.getKey(key));
            }

            this._cacheKeys = [];
            this.setIndicesToCache();


        }

    }
}