﻿declare module Core {
    export interface ICache {
        set<T>(key: string, value: T): T;
        get<T>(key: string): T;
        has(key: string): boolean;
        remove(key: string): void;
        clear(): void;
    }
}