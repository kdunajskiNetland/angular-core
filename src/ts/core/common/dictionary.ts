﻿/// <reference path="../../../../typings/tsd.d.ts" />

module Core {
    export class Dictionary<T> implements IDictionary<T, number> {
        private _map: any = {};
        private _keys: number[] = [];
        private _values: T[] = [];

        set = (key: number, value: T): T => {
            var item = this._map[key];

            this._map[key] = value;

            if (item == null) {
                this._keys.push(key);

                this._values.push(value);
            } else {
                var index = this._keys.indexOf(key);

                if (index != -1) {
                    this._values[index] = value;
                }
            }

            return value;
        }

        get = (key: number, copy?: boolean): T => {
            if (copy) {
                return $.extend(true, {}, this._map[key]);
            } else {
                return this._map[key];

            }
        }

        has = (key: number): boolean => {
            return this._map[key] ? true : false;
        }

        keys = (): number[] => {
            return this._keys;
        }

        values = (): T[] => {
            return this._values;
        }

        remove = (key: number): T => {
            var item = this._map[key];

            if (item == null) return;

            this._map[key] = null;

            var index = this._values.indexOf(item);

            if (index == -1) return;

            this._keys.splice(index, 1);

            return this._values.splice(index, 1)[0];
        }

        clear = (): void => {
            this._map = Object.create(null);

            this._values = [];

            this._keys = [];
        }
    }
}