﻿/// <reference path="common/guid.ts" />
/// <reference path="common/stringdictionary.ts" />
/// <reference path="common/storagedictionary.ts" />
/// <reference path="common/storagedictionarystring.ts" />
/// <reference path="common/dictionary.ts" />
/// <reference path="common/color.ts" />
/// <reference path="common/externaldata.ts" />
/// <reference path="common/iCache.d.ts" />
/// <reference path="common/localStorageCache.ts" />
/// <reference path="common/inMemoryCache.ts" />
declare module Core {
    
    export interface IDictionary<T,K> {
        set(key: K, value: T): T;

        get(key: K, copy?: boolean): T;

        has(key: K): boolean;

        keys(): K[];

        values(): T[];

        remove(key: K): T;

        clear(): void;
    }
}