/// <reference path="../../common.d.ts" />
declare module Core {
    class StorageDictionaryString<T> implements IDictionary<T, string> {
        static SEPARATOR: string;
        static STORAGENAME: string;
        static INSTANCES: {
            [name: string]: StorageDictionaryString<any>;
        };
        private _map;
        private _keys;
        private _values;
        private _cacheKeys;
        private localCache;
        private enityName;
        private idsKey;
        private userPrefix;
        private deserializationFunction;
        constructor(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T);
        static getInstance: <T>(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T) => StorageDictionaryString<T>;
        static generateInstanceName: (userId: number | string, listName: string, entityName: string) => string;
        private setIndicesToCache;
        private getIndices;
        private getKey;
        private loadEntityFromCache;
        set: (key: string, value: T, addToCache?: boolean) => T;
        get: (key: string) => T;
        has: (key: string) => boolean;
        keys: () => string[];
        values: () => T[];
        remove: (key: string, removeFromCache?: boolean) => T;
        clear: () => void;
    }
}
