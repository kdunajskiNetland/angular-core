/// <reference path="../../common.d.ts" />
declare module Core {
    class StorageDictionary<T> implements IDictionary<T, number> {
        static SEPARATOR: string;
        static STORAGENAME: string;
        static INSTANCES: {
            [name: string]: StorageDictionary<any>;
        };
        private _map;
        private _keys;
        private _values;
        private _cacheKeys;
        private localCache;
        private enityName;
        private idsKey;
        private userPrefix;
        private deserializationFunction;
        constructor(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T);
        static getInstance: <T>(userId: number | string, listName: string, entityName: string, deserializationFunction?: (obj: any) => T) => StorageDictionary<T>;
        static generateInstanceName: (userId: number | string, listName: string, entityName: string) => string;
        private setIndicesToCache;
        private getIndices;
        private getKey;
        private loadEntityFromCache;
        set: (key: number, value: T, addToCache?: boolean) => T;
        get: (key: number) => T;
        has: (key: number) => boolean;
        keys: () => number[];
        values: () => T[];
        remove: (key: number, removeFromCache?: boolean) => T;
        clear: () => void;
    }
}
