/// <reference path="../../../../typings/tsd.d.ts" />
declare module Core {
    class StringDictionary<T> implements IDictionary<T, string> {
        private _map;
        private _keys;
        private _values;
        set: (key: string, value: T) => T;
        get: (key: string, copy?: boolean) => T;
        has: (key: string) => boolean;
        keys: () => string[];
        values: () => T[];
        remove: (key: string) => T;
        clear: () => void;
    }
}
