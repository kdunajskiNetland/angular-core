declare module Core {
    class Color {
        r: number;
        g: number;
        b: number;
        constructor(r: number, g: number, b: number);
        /**
        * @param {ratio:number} zakres 0 - 1
        */
        changeValue: (ratio: number) => void;
        toHex: () => string;
        static fromHex: (hex: string) => Color;
    }
}
